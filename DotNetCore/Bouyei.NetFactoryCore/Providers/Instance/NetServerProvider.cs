﻿/*-------------------------------------------------------------
 *   auth: bouyei
 *   date: 2017/7/29 13:43:40
 *contact: 453840293@qq.com
 *profile: www.openthinking.cn
 *   guid: 7b7a759e-571f-4486-969a-5306e9dc0f51
---------------------------------------------------------------*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bouyei.NetFactoryCore
{
    using Tcp;
    using Udp;
    public class NetServerProvider : INetServerProvider
    {
        #region variable
        private TcpServerProvider tcpServerProvider = null;
        private UdpServerProvider udpServerProvider = null;
        private int chunkBufferSize = 4096;
        private int maxNumberOfConnections = 512;
        private bool _isDisposed = false;
        #endregion

        #region property
        private OnReceiveHandler _receiveHanlder = null;
        public OnReceiveHandler ReceiveHandler
        {
            get { return _receiveHanlder; }
            set
            {
                _receiveHanlder = value;
                if (NetProviderType.Tcp == NetProviderType)
                {
                    tcpServerProvider.ReceivedCallback = _receiveHanlder;
                }
                else if (NetProviderType.Udp == NetProviderType)
                {
                    udpServerProvider.ReceiveCallbackHandler = _receiveHanlder;
                }
            }
        }

        private OnSentHandler _sentHanlder = null;
        public OnSentHandler SentHandler
        {
            get { return _sentHanlder; }
            set
            {
                _sentHanlder = value;
                if (NetProviderType.Tcp == NetProviderType)
                {
                    tcpServerProvider.SentCallback = _sentHanlder;
                }
                else if (NetProviderType.Udp == NetProviderType)
                {
                    udpServerProvider.SentCallbackHandler = _sentHanlder;
                }
            }
        }

        private OnAcceptHandler _acceptHanlder = null;
        public OnAcceptHandler AcceptHandler
        {
            get { return _acceptHanlder; }
            set
            {
                _acceptHanlder = value;
                if (NetProviderType.Tcp == NetProviderType)
                {
                    tcpServerProvider.AcceptedCallback = _acceptHanlder;
                }
            }
        }

        private OnReceiveOffsetHandler _receiveOffsetHandler = null;
        public OnReceiveOffsetHandler ReceiveOffsetHandler
        {
            get { return _receiveOffsetHandler; }
            set
            {
                _receiveOffsetHandler = value;
                if (NetProviderType.Tcp == NetProviderType)
                {
                    tcpServerProvider.ReceiveOffsetCallback = _receiveOffsetHandler;
                }
                else if (NetProviderType.Udp == NetProviderType)
                {
                    udpServerProvider.ReceiveOffsetHanlder = _receiveOffsetHandler;
                }
            }
        }

        private OnDisconnectedHandler _disconnectedHanlder = null;
        public OnDisconnectedHandler DisconnectedHandler
        {
            get { return _disconnectedHanlder; }
            set
            {
                _disconnectedHanlder = value;
                if (NetProviderType.Tcp == NetProviderType)
                {
                    tcpServerProvider.DisconnectedCallback = _disconnectedHanlder;
                }
                else if (NetProviderType.Udp == NetProviderType)
                {
                    udpServerProvider.DisconnectedCallbackHandler = _disconnectedHanlder;
                }
            }
        }

        public NetProviderType NetProviderType { get; private set; }

        #endregion

        #region constructor
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool isDisposing)
        {
            if (_isDisposed) return;

            if (isDisposing)
            {
                if (tcpServerProvider != null)
                    tcpServerProvider.Dispose();

                if (udpServerProvider != null)
                    udpServerProvider.Dispose();
                _isDisposed = true;
            }
        }

        public NetServerProvider(
            int chunkBufferSize = 4096,
            int maxNumberOfConnections = 64,
            NetProviderType netProviderType = NetProviderType.Tcp)
        {
            this.NetProviderType = netProviderType;
            this.chunkBufferSize = chunkBufferSize;
            this.maxNumberOfConnections = maxNumberOfConnections;

            if (netProviderType == NetProviderType.Tcp)
            {
                tcpServerProvider = new TcpServerProvider(maxNumberOfConnections, chunkBufferSize);
            }
            else if (netProviderType == NetProviderType.Udp)
            {
                udpServerProvider = new UdpServerProvider();
            }
            else if (netProviderType == NetProviderType.WebSocket)
            {

            }
        }

        public NetServerProvider(NetProviderType netProviderType)
        {
            this.NetProviderType = netProviderType;

            if (netProviderType == NetProviderType.Tcp)
            {
                tcpServerProvider = new TcpServerProvider(maxNumberOfConnections, chunkBufferSize);
            }
            else if (netProviderType == NetProviderType.Udp)
            {
                udpServerProvider = new UdpServerProvider();
            }
            else if (netProviderType == NetProviderType.WebSocket)
            {

            }
        }

        public static NetServerProvider CreateProvider(
            int chunkBufferSize = 4096,
            int maxNumberOfConnections = 64,
            NetProviderType netProviderType = NetProviderType.Tcp)
        {
            return new NetServerProvider(chunkBufferSize, maxNumberOfConnections, netProviderType);
        }

        #endregion

        #region public method
        public bool Start(int port, string ip = "0.0.0.0")
        {
            if (NetProviderType == NetProviderType.Tcp)
            {
                return tcpServerProvider.Start(port, ip);
            }
            else if (NetProviderType == NetProviderType.Udp)
            {
                udpServerProvider.Start(port, chunkBufferSize, maxNumberOfConnections);
                return true;
            }
            else if (NetProviderType == NetProviderType.WebSocket)
            {

            }
            return false;
        }

        public void Stop()
        {
            if (NetProviderType == NetProviderType.Tcp)
            {
                tcpServerProvider.Stop();
            }
            else if (NetProviderType == NetProviderType.Udp)
            {
                udpServerProvider.Stop();
            }
            else if (NetProviderType == NetProviderType.WebSocket)
            {

            }
        }

        public bool Send(SegmentOffsetToken segToken, bool waiting = true)
        {
            if (NetProviderType == NetProviderType.Tcp)
            {
                return tcpServerProvider.Send(segToken, waiting);
            }
            else if (NetProviderType == NetProviderType.Udp)
            {
                return udpServerProvider.Send(segToken.dataSegment,segToken.sToken.TokenIpEndPoint, waiting);
            }
            else if (NetProviderType == NetProviderType.WebSocket)
            {

            }
            return false;
        }

        public int SendSync(SegmentOffsetToken segToken)
        {
            if (NetProviderType == NetProviderType.Tcp)
            {
                tcpServerProvider.SendSync(segToken);
            }
            else if (NetProviderType == NetProviderType.Udp)
            {
                return udpServerProvider.SendSync(
                      segToken.sToken.TokenIpEndPoint, segToken.dataSegment);
            }
            return 0;
        }

        public void CloseToken(SocketToken sToken)
        {
            if (NetProviderType == NetProviderType.Tcp)
            {
                tcpServerProvider.Close(sToken);
            }
            else if (NetProviderType == NetProviderType.Udp)
            {
               
            }
            else if (NetProviderType == NetProviderType.WebSocket)
            {

            }
        }
        #endregion
    }
}
