﻿using System;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System.Collections.Generic;

namespace Bouyei.NetFactoryCore.Udp
{
    using Base;
    using Providers.Udp;

    public class UdpClientProvider : UdpSocket,IDisposable
    {
        #region 定义变量
        private bool _isDisposed = false;
        private int bufferSizeByConnection = 4096;
        private int maxNumberOfConnections = 64;

        private LockParam lParam = new LockParam();
        private ManualResetEvent mReset = new ManualResetEvent(false);
        private SocketTokenManager<SocketAsyncEventArgs> sendTokenManager = null;
        private SocketBufferManager sendBufferManager = null;

        #endregion

        #region 属性
        public int SendBufferPoolNumber { get { return sendTokenManager.Count; } }

        /// <summary>
        /// 接收回调处理
        /// </summary>
        public OnReceiveHandler ReceiveCallbackHandler { get; set; }

        /// <summary>
        /// 发送回调处理
        /// </summary>
        public OnSentHandler SentCallbackHandler { get; set; }
        /// <summary>
        /// 接收缓冲区回调
        /// </summary>
        public OnReceiveOffsetHandler ReceiveOffsetHandler { get; set; }
        #endregion

        #region public method
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool isDisposing)
        {
            if (_isDisposed) return;

            if (isDisposing)
            {
                DisposeSocketPool();
                SafeClose();
                
                _isDisposed = true;
            }
        }

        private void DisposeSocketPool()
        {
            sendTokenManager.ClearToCloseArgs();
            if (sendBufferManager != null)
            {
                sendBufferManager.Clear();
            }
        }

        /// <summary>
        /// 构造方法
        /// </summary>
        public UdpClientProvider(int bufferSizeByConnection, int maxNumberOfConnections)
            :base(bufferSizeByConnection)
        {
            this.maxNumberOfConnections = maxNumberOfConnections;
            this.bufferSizeByConnection = bufferSizeByConnection;
            Initialize();
        }

        public void Disconnect()
        {
            Close();
            isConnected = false;
        }

        /// <summary>
        /// 尝试连接
        /// </summary>
        /// <param name="port"></param>
        /// <param name="ip"></param>
        /// <returns></returns>
        public bool Connect(int port, string ip)
        {
            Close();

            CreateUdpSocket(port, ip);

            int retry = 3;
            again:
            try
            {
                //探测是否有效连接
                SocketAsyncEventArgs sArgs = new SocketAsyncEventArgs();
                sArgs.Completed += IO_Completed;
                sArgs.UserToken = socket;
                sArgs.RemoteEndPoint =ipEndPoint;
                sArgs.SetBuffer(new byte[] { 0 }, 0, 1);

                bool rt = socket.SendToAsync(sArgs);
                if (rt)
                {
                    StartReceive();
                    mReset.WaitOne();
                }
            }
            catch (Exception ex)
            {
                retry -= 1;
                if (retry > 0)
                {
                    Thread.Sleep(1000);
                    goto again;
                }
                throw ex;
            }
            return isConnected;
        }

 
        public bool Send(SegmentOffset sendSegment, bool waiting = true)
        {
            try
            {
                bool isWillEvent = true;
                ArraySegment<byte>[] segItems = sendBufferManager.BufferToSegments(sendSegment.buffer, sendSegment.offset, sendSegment.size);
                foreach (var seg in segItems)
                {
                    SocketAsyncEventArgs tArgs = sendTokenManager.GetEmptyWait((retry) =>
                    {
                        return true;
                    }, waiting);

                    if (tArgs == null)
                        throw new Exception("发送缓冲池已用完,等待回收...");

                    tArgs.RemoteEndPoint = ipEndPoint;

                    if (!sendBufferManager.WriteBuffer(tArgs, seg.Array, seg.Offset, seg.Count))
                    {
                        sendTokenManager.Set(tArgs);

                        throw new Exception(string.Format("发送缓冲区溢出...buffer block max size:{0}", sendBufferManager.BlockSize));
                    }

                    isWillEvent &= socket.SendToAsync(tArgs);
                    if (!isWillEvent)
                    {
                        ProcessSent(tArgs);
                    }
                }
                return isWillEvent;
            }
            catch (Exception ex)
            {
                Close();

                throw ex;
            }
        }

        /// <summary>
        /// 同步发送
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="recAct"></param>
        /// <param name="recBufferSize"></param>
        /// <returns></returns>
        public int SendSync(SegmentOffset sendSegment, SegmentOffset receiveSegment)
        {
            int sent = socket.SendTo(sendSegment.buffer, sendSegment.offset, sendSegment.size, 0, ipEndPoint);
            if (receiveSegment == null
                || receiveSegment.buffer == null
                || receiveSegment.size == 0) return sent;

            int cnt = socket.ReceiveFrom(receiveSegment.buffer,
                receiveSegment.size,
                SocketFlags.None,
                ref ipEndPoint);

            return sent;
        }

        /// <summary>
        /// 同步接收
        /// </summary>
        /// <param name="recAct"></param>
        /// <param name="recBufferSize"></param>
        public void ReceiveSync(SegmentOffset receiveSegment, Action<SegmentOffset> receiveAction)
        {
            int cnt = 0;
            do
            {
                cnt = socket.ReceiveFrom(receiveSegment.buffer,
                    receiveSegment.size,
                    SocketFlags.None,
                    ref ipEndPoint);

                if (cnt <= 0) break;

                receiveAction(receiveSegment);
            } while (true);
        }

        /// <summary>
        /// 开始接收数据
        /// </summary>
        /// <param name="remoteEP"></param>
        public void StartReceive()
        {
            using (LockWait lwait = new LockWait(ref lParam))
            {
                SocketAsyncEventArgs sArgs = new SocketAsyncEventArgs();
                sArgs.Completed += IO_Completed;
                sArgs.UserToken = socket;
                sArgs.RemoteEndPoint = ipEndPoint;
                sArgs.SetBuffer(receiveBuffer, 0, bufferSizeByConnection);
                if (!socket.ReceiveFromAsync(sArgs))
                {
                    ProcessReceive(sArgs);
                }
            }
        }

        #endregion

        #region private method
        /// <summary>
        /// 初始化对象
        /// </summary>
        /// <param name="recBufferSize"></param>
        /// <param name="port"></param>
        private void Initialize()
        {
            sendTokenManager = new SocketTokenManager<SocketAsyncEventArgs>(maxNumberOfConnections);
            sendBufferManager = new SocketBufferManager(maxNumberOfConnections, bufferSizeByConnection);

            //初始化发送接收对象池
            for (int i = 0; i < maxNumberOfConnections; ++i)
            {
                SocketAsyncEventArgs sendArgs = new SocketAsyncEventArgs();
                sendArgs.Completed += IO_Completed;
                sendArgs.UserToken = socket;
                sendBufferManager.SetBuffer(sendArgs);
                sendTokenManager.Set(sendArgs);
            }
        }

        private void Close()
        {
            if (socket != null)
            {
                socket.Shutdown(SocketShutdown.Both);
                socket.Close();
                socket.Dispose();
            }
        }

        private void ProcessReceive(SocketAsyncEventArgs e)
        {
            using (LockWait lwait = new LockWait(ref lParam))
            {
                SocketToken sToken = new SocketToken()
                {
                    TokenSocket = e.UserToken as Socket,
                    TokenIpEndPoint = (IPEndPoint)e.RemoteEndPoint
                };

                try
                {
                    if (e.SocketError != SocketError.Success || e.BytesTransferred == 0)
                        return;

                    //初次连接心跳
                    if (isServerResponse(e) == false)
                        return;

                    //缓冲区偏移量返回
                    if (ReceiveOffsetHandler != null)
                        ReceiveOffsetHandler(sToken, e.Buffer, e.Offset, e.BytesTransferred);

                    //截取后返回
                    if (ReceiveCallbackHandler != null)
                    {
                        if (e.Offset == 0 && e.BytesTransferred == e.Buffer.Length)
                        {
                            ReceiveCallbackHandler(sToken, e.Buffer);
                        }
                        else
                        {
                            byte[] realbytes = new byte[e.BytesTransferred];
                            Buffer.BlockCopy(e.Buffer, e.Offset, realbytes, 0, e.BytesTransferred);

                            ReceiveCallbackHandler(sToken, realbytes);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    if (e.SocketError == SocketError.Success)
                    {
                        //继续下一个接收
                        if (!sToken.TokenSocket.ReceiveFromAsync(e))
                        {
                            ProcessReceive(e);
                        }
                    }
                }
            }
        }

        private void ProcessSent(SocketAsyncEventArgs e)
        {
            try
            {
                isConnected = e.SocketError == SocketError.Success;

                if (SentCallbackHandler != null && isClientRequest(e) == false)
                {
                    SocketToken sToken = new SocketToken()
                    {
                        TokenSocket = e.UserToken as Socket,
                        TokenIpEndPoint = (IPEndPoint)e.RemoteEndPoint
                    };
                    SentCallbackHandler(sToken, e.Buffer, e.Offset, e.BytesTransferred);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                sendTokenManager.Set(e);
            }
        }

        void IO_Completed(object sender, SocketAsyncEventArgs e)
        {
            switch (e.LastOperation)
            {
                case SocketAsyncOperation.ReceiveFrom:
                    ProcessReceive(e);
                    break;
                case SocketAsyncOperation.SendTo:
                    ProcessSent(e);
                    break;
            }
        }

        private bool isServerResponse(SocketAsyncEventArgs e)
        {
            isConnected = e.SocketError == SocketError.Success;

            if (e.BytesTransferred == 1 && e.Buffer[0] == 1)
            {
                mReset.Set();
                return true;
            }
            else return false;
        }

        private bool isClientRequest(SocketAsyncEventArgs e)
        {
            if (e.BytesTransferred == 1 && e.Buffer[0] == 0)
            {
                return true;
            }
            else return false;
        }
        #endregion
    }
}