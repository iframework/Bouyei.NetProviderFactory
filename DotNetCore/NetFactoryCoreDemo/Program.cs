﻿using System;
using System.Text;
using System.Threading;
using Bouyei.NetFactoryCore;

namespace NetFactoryCoreDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            TcpDemo();
        }

        private static void TcpDemo()
        {
            int port = 12346;
            //服务端
            INetServerProvider serverSocket = NetServerProvider.CreateProvider();
            
            serverSocket.ReceiveOffsetHandler = new OnReceiveOffsetHandler((sToken, buff, offset, count) =>
            {
                try
                {
                    string info = Encoding.UTF8.GetString(buff, offset, count);
                    Console.WriteLine(info);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            });
            serverSocket.AcceptHandler = new OnAcceptHandler((sToken) =>
            {
                serverSocket.Send(new SegmentOffsetToken()
                {
                    sToken = sToken,
                    dataSegment = new SegmentOffset()
                    {
                        buffer = Encoding.Default.GetBytes("welcome" + DateTime.Now.ToString())
                    }
                }, false);

                Console.WriteLine("accept" + sToken.TokenIpEndPoint);
            });

            serverSocket.DisconnectedHandler = new OnDisconnectedHandler((stoken) =>
            {
                Console.WriteLine("disconnect" + stoken.TokenId);
            });

            bool isOk = serverSocket.Start(port);
            if (isOk)
            {
                Console.WriteLine("已启动服务。。。");

                //客户端
                INetClientProvider clientSocket = NetClientProvider.CreateProvider();

                //异步连接
                clientSocket.ReceiveOffsetHandler = new OnReceiveOffsetHandler((sToken, buff, offset, count) =>
                {
                    try
                    {
                       Console.WriteLine("rec:" + Encoding.Default.GetString(buff,offset,count));
                    }
                    catch (Exception ex)
                    {

                    }
                });
                clientSocket.DisconnectedHandler = new OnDisconnectedHandler((stoken) =>
                {
                    Console.WriteLine("clinet discount");
                });
                again:
                bool rt = clientSocket.ConnectTo(port, "127.0.0.1");/* 10.152.0.71*/
                if (rt)
                {
                    for (int i = 0; i < 10000; i++)
                    {
                         Thread.Sleep(50);
                        if (i % 100 == 0)
                        {
                            Console.WriteLine(clientSocket.BufferPoolCount + ":" + i);
                        }
                        bool isTrue = clientSocket.Send(new SegmentOffset(Encoding.Default.GetBytes("hello"+DateTime.Now)), false);
                        //if (isTrue == false) break;
                        //break;
                    }
                    //byte[] buffer = System.IO.File.ReadAllBytes("TRANSACTION_EXTRANSACTIONUPLOAD_REQ_52_1000_20171031143825836.json");

                    //clientSocket.Send(buffer);

                    //Console.WriteLine("complete:sent:" + sentlength.ToString() + "rec:" + reclength.ToString());
                    int ab = 0;
                    while (true)
                    {
                         Thread.Sleep(3000);
                        Console.WriteLine("retry :pool:"+clientSocket.BufferPoolCount);
                        if (ab++ >= 1) break;
                    }
                    var c = Console.ReadKey();
                    if (c.KeyChar == 'r') goto again;

                    clientSocket.Dispose();
                }
            }
            Console.ReadKey();
            serverSocket.Dispose();
        }
    }
}
